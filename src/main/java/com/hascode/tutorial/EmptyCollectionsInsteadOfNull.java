package com.hascode.tutorial;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class EmptyCollectionsInsteadOfNull {
	public static void main(final String[] args) {
		new EmptyCollectionsInsteadOfNull().run();
	}

	public void run() {
		BookmarkProvider bookmarkProvider = new BookmarkProvider();
		List<URL> bookmarks = bookmarkProvider.getBookmarks();
		// bookmarks might be null so we have to check ..
		if (bookmarks != null) {
			for (URL url : bookmarks) {
				System.out.println(url);
			}
		}

		List<URL> favourite = bookmarkProvider.getFavourites();
		// favourite is never null so we don't need a check here
		for (URL url : favourite) {
			System.out.println(url); // we never get here because
										// Collection.isEmpty is checked
		}

	}

	class BookmarkProvider {
		public List<URL> getBookmarks() {
			// no bookmarks? returning null ..
			return null; // bad
		}

		public List<URL> getFavourites() {
			// no favourites? returning an empty collection..
			return new ArrayList<URL>(); // better
		}
	}
}
