package com.hascode.tutorial;

import com.google.common.base.Preconditions;

public class CheckingNullPreconditions {
	public static void main(final String[] args) {
		new CheckingNullPreconditions().run();
	}

	public void run() {
		String userName = getUsernameById(42l);
		System.out.println(userName);

		String nullName = getUsernameById(null);
		System.out.println(nullName);
	}

	public String getUsernameById(final Long id) {
		Preconditions.checkNotNull(id);
		return "user_" + id;
	}
}
