package com.hascode.tutorial;

/*
 * run with assertions enabled by adding the parameter -ea
 */
public class UsingJavaAssertions {
	public static void main(final String[] args) {
		System.out.println(findTagById(null));

		/*
		 * Exception in thread "main" java.lang.AssertionError: id must not be
		 * null! at
		 * com.hascode.tutorial.UsingJavaAssertions.findTagById(UsingJavaAssertions
		 * .java:12) at
		 * com.hascode.tutorial.UsingJavaAssertions.main(UsingJavaAssertions
		 * .java:8)
		 */
	}

	static String findTagById(final Long id) {
		assert id != null : "id must not be null!";
		return "label_" + Long.toHexString(id);
	}
}
