package com.hascode.tutorial;

import javax.annotation.Nullable;

public class FindBugsNullMarker {
	public static void main(final String[] args) {
		System.out.println(findTagById(1024l)); // label_400
		System.out.println(findTagByIdNullAllowed(null)); // ok, we've declared
															// @Nullable
		System.out.println(findTagById(null)); // findbugs complains
	}

	static String findTagById(final Long id) {
		return "label_" + Long.toHexString(id);
	}

	static String findTagByIdNullAllowed(@Nullable final Long id) {
		return findTagById(id); // findbugs complains that id must be notnull
								// but is marked nullable
	}

}
