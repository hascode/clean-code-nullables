package com.hascode.tutorial;

import java.util.Locale;

public class NullObjectPatternExample {
	interface PostManager {
		void sendMail(String from, String to, String message);
	}

	class PostManagerImpl implements PostManager {
		@Override
		public void sendMail(final String from, final String to,
				final String message) {
			handleMailProcessing(from, to, message); // do real work
		}

		private void handleMailProcessing(final String from, final String to,
				final String message) {
		}
	}

	class NullPostManager implements PostManager {
		@Override
		public void sendMail(final String from, final String to,
				final String message) {
			// do nothing
		}
	}

	class PostManagerRepository {
		public PostManager managerForLocale(final Locale locale) {
			return (locale.equals(Locale.GERMAN)) ? new NullPostManager()
					: new PostManagerImpl();
		}
	}

	public static void main(final String[] args) {
		new NullObjectPatternExample().run();
	}

	private void run() {
		PostManagerRepository pmr = new PostManagerRepository();
		PostManager p1 = pmr.managerForLocale(Locale.ENGLISH);
		p1.sendMail("someone@hascode.com", "someother@hascode.com",
				"Helo there");
		PostManager p2 = pmr.managerForLocale(Locale.GERMAN);
		p2.sendMail("someone@hascode.com", "someother@hascode.com",
				"Helo there");
	}

}
