package com.hascode.tutorial;

import com.google.common.base.Optional;

import fj.data.Option;

public class OptionalInsteadOfNulls {

	public static void main(final String[] args) {
		new OptionalInsteadOfNulls().run();
	}

	public void run() {
		// the guava way
		Optional<RSSFeed> feed = fetchFeedFor("http://www.hascode.com/feed/");
		if (feed.isPresent()) {
			// we have a feed .. do stuff with it ..
			RSSFeed f = feed.get();
			// ...
		}

		// the functional java way
		Option<RSSFeed> feed2 = feedFor("http://www.hascode.com/feed/");
		if (feed2.isSome()) {
			// we have a feed .. do stuff with it ..
			RSSFeed f = feed2.some();
			// ...
		}

		// or
		if (feed2.isNone()) {
			// no feed ..
		}
	}

	// the guava way
	Optional<RSSFeed> fetchFeedFor(final String url) {
		try {
			// do some stuff
			RSSFeed feed = new RSSFeed();
			return Optional.of(feed);
		} catch (Exception e) {
			// error case
			return Optional.absent();
		}
	}

	Option<RSSFeed> feedFor(final String url) {
		try {
			// do some stuff
			RSSFeed feed = new RSSFeed();
			return Option.some(feed);
		} catch (Exception e) {
			// error case
			return Option.none();
		}
	}

	class RSSFeed {
	}
}
