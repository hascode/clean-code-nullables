# Clean Code - Handling Null Values

Some examples how to avoid using null values in a Java project.

Please feel free to take a look at the full tutorial at [www.hascode.com].

---

**2013 Micha Kops / hasCode.com**

   [www.hascode.com]:http://www.hascode.com/